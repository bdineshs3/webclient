FROM openjdk:8
EXPOSE 8080
ADD target/ordermangement.jar ordermangement.jar
ENTRYPOINT ["java","-jar","/ordermangement.jar"]