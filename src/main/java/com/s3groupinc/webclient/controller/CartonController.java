package com.s3groupinc.webclient.controller;

import com.s3groupinc.webclient.model.CartonResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CartonController {
  @Autowired
  private WebClient webClient;

  @GetMapping("/carton")
  public CartonResource getCartonAll() {
    return webClient.getCartonAll();
  }

  @GetMapping("/carton/{chPickTicketControlNumber}")
  public CartonResource getCartonList(@PathVariable String chPickTicketControlNumber) {
    return webClient.getCartonList(chPickTicketControlNumber);
  }
}
