package com.s3groupinc.webclient.controller;

import com.s3groupinc.webclient.model.CartonResource;
import com.s3groupinc.webclient.model.PickTicketResource;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ordermanagement")
public interface WebClient {
  @GetMapping("/ticket/{phPickTicketControlNumber}")
  PickTicketResource getPickTicketList(@PathVariable("phPickTicketControlNumber") String phPickTicketControlNumber);

  @GetMapping("/ticket")
  PickTicketResource getPickTicetAll();

  @GetMapping("/order/{phOrderNumber}")
  PickTicketResource getOrderDetails(@PathVariable ("phOrderNumber") String phOrderNumber);

  @GetMapping("/carton/{chPickTicketControlNumber}")
  CartonResource getCartonList(@PathVariable String chPickTicketControlNumber);

  @GetMapping("/carton")
  CartonResource getCartonAll();
}
