package com.s3groupinc.webclient.controller;

import com.s3groupinc.webclient.model.PickTicket;
import com.s3groupinc.webclient.model.PickTicketResource;
import com.s3groupinc.webclient.repository.PickTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PickTicketController {
  @Autowired
  private WebClient webClient;


  @GetMapping("/ticket/{phPickTicketControlNumber}")
  public PickTicketResource getPickTicketAll(@PathVariable("phPickTicketControlNumber") String phPickTicketControlNumber) {
    return webClient.getPickTicketList(phPickTicketControlNumber);
  }

  @GetMapping("/order/{phOrderNumber}")
  public PickTicketResource getOrderDetails(@PathVariable String phOrderNumber) {
    return webClient.getOrderDetails(phOrderNumber);
  }

  @GetMapping("/ticket")
  public PickTicketResource getPickTicketList() {
    return webClient.getPickTicetAll();
  }
}
