package com.s3groupinc.webclient.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class PickTicketResource {
  private List<PickTicket> pickTicketList;
}
