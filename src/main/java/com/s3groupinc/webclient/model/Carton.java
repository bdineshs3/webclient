package com.s3groupinc.webclient.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Component
public class Carton {
  @Id
  @NotNull
  private String chPickTicketControlNumber;

  private String chCartonNumber;

  private String chTotalQuantity;

  private String chStatusFlag;

  private String chTrackingNumber;
}