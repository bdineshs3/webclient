package com.s3groupinc.webclient.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Component
public class PickTicket {
  @Id
  @NotNull
  private String phPickTicketControlNumber;

  private String phCustomerPurchaseOrderNumber;

  private String phOrderNumber;

  private String phShipTo;

  private String phShipToName;

  private String phStartShipDate;

  private String phStopShipDate;

  private String phTotalNumberOfUnits;

  private String phPickTicketStatus;
}