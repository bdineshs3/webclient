package com.s3groupinc.webclient;

import com.s3groupinc.webclient.controller.WebClient;
import com.s3groupinc.webclient.model.Carton;
import com.s3groupinc.webclient.model.CartonResource;
import com.s3groupinc.webclient.model.PickTicket;
import com.s3groupinc.webclient.model.PickTicketResource;
import com.s3groupinc.webclient.repository.CartonRepository;
import com.s3groupinc.webclient.repository.PickTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
class DataSetup implements ApplicationRunner {
/*
  @Autowired
  private PickTicketRepository pickTicketRepository;

  @Autowired
  private CartonRepository cartonRepository;

  @Autowired
  private WebClient webClient;
*/

  @Override
  public void run(ApplicationArguments args) {
 /*   PickTicketResource pickTicketResource = webClient.getPickTicetAll();
    List<PickTicket> pickTicketList = pickTicketResource.getPickTicketList();
    int size = pickTicketList.size();

    for (int i = 0; i < size; i++) {
      pickTicketRepository.save(pickTicketList.get(i));
    }

    CartonResource cartonResource = webClient.getCartonAll();
    List<Carton> cartonList = cartonResource.getCartonList();
    size = cartonList.size();

    for (int i = 0; i < size; i++) {
      cartonRepository.save(cartonList.get(i));
    }
 */ }
}

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class DemoApplication {
  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }
}
