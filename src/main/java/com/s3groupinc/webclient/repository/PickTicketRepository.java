package com.s3groupinc.webclient.repository;

import com.s3groupinc.webclient.model.PickTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PickTicketRepository extends JpaRepository<PickTicket, String> {
  List<PickTicket> findByphPickTicketControlNumber(String phPickTicketControlNumber);
  List<PickTicket> findByphOrderNumber(String phOrderNumber);
}
