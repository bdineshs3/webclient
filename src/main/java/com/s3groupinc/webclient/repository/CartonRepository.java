package com.s3groupinc.webclient.repository;

import com.s3groupinc.webclient.model.Carton;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CartonRepository extends JpaRepository<Carton, String> {
  List<Carton> findBychPickTicketControlNumber(String chpctl);
}  
